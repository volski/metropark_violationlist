package il.co.avpro.metropark_violationlist;

import android.graphics.Color;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private static final String JSON_URL = "https://data.gov.il/api/action/datastore_search?resource_id=a2dd714b-e012-46e4-a996-6307f8b46fc5";
    ListView listView;
    List<Violation> violationList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        listView = (ListView) findViewById(R.id.listView);
        violationList = new ArrayList<>();

        loadFromAPI();

        final SwipeRefreshLayout refresh = (SwipeRefreshLayout) findViewById(R.id.refresh);
        refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadFromAPI();
                refresh.setRefreshing(false);
            }
        });
    }

    private void loadFromAPI(){
        StringRequest stringRequest = new StringRequest(Request.Method.GET, JSON_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        loadViolationList(response,true);
                        mCreateAndSaveFile(response,"backup");
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                        mReadJsonData("backup");
                    }
                });
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        requestQueue.add(stringRequest);
    }

    public void mCreateAndSaveFile(String mJsonResponse,String fileName) {
        try {
            File file = new File(getApplicationContext().getFilesDir(), fileName);
            FileWriter fw = new FileWriter(file);
            fw.write(mJsonResponse);
            fw.flush();
            fw.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception w) {
            w.printStackTrace();
        }
    }
    public void mReadJsonData(String fileName) {
        String params = fileName;
        String mResponse = "";
        try {
            File f = new File(getApplicationContext().getFilesDir(), params);
            FileInputStream is = new FileInputStream(f);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            mResponse = new String(buffer);
        } catch (IOException e) {
            e.printStackTrace();
        }
        catch (Exception w)
        {
            w.printStackTrace();
        }
        loadViolationList(mResponse,false);
    }




    private void loadViolationList(String response,Boolean isOnLine) {
        final ProgressBar progressBar = (ProgressBar) findViewById(R.id.progressBar);

        progressBar.setVisibility(View.INVISIBLE);
        TextView view_status = (TextView) findViewById(R.id.status);
        if(isOnLine) {
            view_status.setText("OnLine");
            view_status.setTextColor(Color.GREEN);
        }else{
            view_status.setText("OffLine");
            view_status.setTextColor(Color.RED);
        }

        try {
            JSONObject obj = new JSONObject(response);
            JSONObject objResult = obj.getJSONObject("result");
            JSONArray violations = objResult.getJSONArray("records");
            for (int i = 0; i < violations.length(); i++) {
                JSONObject violatoin = violations.getJSONObject(i);
                Violation violationRecord = new Violation(violatoin.getString("_id"), violatoin.getString("סמל עבירה"),violatoin.getString("תיאור עבירה"),violatoin.getString("ניקוד"));
                violationList.add(violationRecord);
            }
            ListViewAdapter adapter = new ListViewAdapter(violationList, getApplicationContext());
            listView.setAdapter(adapter);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
