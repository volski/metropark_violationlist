package il.co.avpro.metropark_violationlist;

public class Violation {
    String id, violationNumber ,violationDescription,violationPoints;

    public Violation(String id, String violationNumber , String violationDescription, String violationPoints) {
        this.id = id;
        this.violationNumber = violationNumber;
        this.violationDescription = violationDescription;
        this.violationPoints = violationPoints;
    }

    public String getId() {
        return id;
    }
    public String getViolationNumber() {
        return violationNumber;
    }
    public String getViolationDescription() {
        return violationDescription;
    }
    public String getViolationPoints() {
        return violationPoints;
    }

}
