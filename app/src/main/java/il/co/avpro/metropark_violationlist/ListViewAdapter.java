package il.co.avpro.metropark_violationlist;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;


public class ListViewAdapter extends ArrayAdapter<Violation> {
    private List<Violation> violationList;
    private Context mCtx;

    public ListViewAdapter(List<Violation> violationList, Context mCtx) {
        super(mCtx, R.layout.list_violation, violationList);
        this.violationList = violationList;
        this.mCtx = mCtx;
    }

    public ListViewAdapter(@androidx.annotation.NonNull Context context, int resource) {
        super(context, resource);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(mCtx);
        View listViewItem = inflater.inflate(R.layout.list_violation, null, true);

        TextView view_ID = listViewItem.findViewById(R.id.ID);
        TextView view_violationNumber = listViewItem.findViewById(R.id.violationNumber);
        TextView view_violationPoints = listViewItem.findViewById(R.id.violationPoints);
        TextView view_violationDescription = listViewItem.findViewById(R.id.violationDescription);

        Violation violation = violationList.get(position);

        view_ID.setText("עבירה מס: " + violation.getId());
        view_violationNumber.setText("קוד עבירה: " +violation.getViolationNumber());
        view_violationPoints.setText("ניקוד עבירה: " + violation.getViolationPoints());
        view_violationDescription.setText(violation.getViolationDescription());

        return listViewItem;
    }
}
